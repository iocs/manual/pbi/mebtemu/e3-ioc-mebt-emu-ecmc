# Pepperl+Fuchs KFD2-GUT-1.D configuration
In order:
1. Reset to ___factory defaults___
1. Change input type to ___4-wire___
1. Change relay 1 to trip on ___MAX___
1. Change relay 1 ___trip value___ ¹²
1. Change relay 1 ___hysteresis___ to 1 °C
1. Change relay 2 ___trip value___ ¹²
1. Change relay 2 ___hysteresis___ to 1 °C
1. Change ___interference current___ to Up/Down
1. Change ___final value___ to 100 °C

¹ decided by the system owner  
² update `${P}-WaterTemp.HIHI` accordingly

# MEBT EMU (Medium Energy Beam Transfer Emittance Measurement Unit) EtherCAT IOC
- [Confluence page](https://confluence.esss.lu.se/x/0rW8G)
- [Interlock strategies](https://confluence.ess.eu/x/8DYAFQ)
- [EPLAN drawing](https://chess.esss.lu.se/enovia/link/ESS-5289404/21308.51166.35132.64692/valid)
- [5202A configuration](doc/5202A.md)
- [KFD2-GUT-1.D configuration](doc/KFD2-GUT-1.D.md)
```
Encoder: ???
Brake: ???
Motor: Nanotec ST4118M1804-B
Gearbox: ???
Limit switches: ???
Home switch: ???
Pulse isolator: PR Electronics 5202A
Temperature transmitter: Pepperl+Fuchs KFD2-GUT-1.D
```
